from datetime import datetime
from airflow import DAG
from airflow.operators.bash_operator import BashOperator



with DAG(
    dag_id="test_dag_20", start_date=datetime(2021, 11, 1), schedule_interval="@daily"
) as dag:
    BashOperator(
        task_id="bash_task_test_dag_20",
        bash_command="echo hello world",
    )
